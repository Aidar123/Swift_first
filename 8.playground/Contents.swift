import UIKit

enum Calculators {
    case Standart, Special
}

enum Operations {
    case multi, divis, plus, minus
    case square, degree, pi, sign
}

protocol Calcul {
    func Chet() -> Double
}

private class Calculator: Calcul {
    private let Number: Double
    private let NumOrDeg: Double
    private let operation: Operations
    private let calculators: Calculators
    init(Number: Double, NumOrDeg: Double, operation: Operations, calculator: Calculators){
        self.Number = Number
        self.NumOrDeg = NumOrDeg
        self.operation = operation
        self.calculators = calculator
    }
    func Chet() -> Double {
        switch calculators {
        case .Standart:
            switch operation {
            case .multi:
                return Number * NumOrDeg
            case .divis:
                return Number / NumOrDeg
            case .plus:
                return Number + NumOrDeg
            case .minus:
                return Number - NumOrDeg
            default:
                print ("Используйте Специальный калькулятор")
                return 0
            }
        case .Special:
            switch operation {
            case .square:
                return pow(Number, (1.0) / NumOrDeg)
            case .degree:
                return pow(Number, NumOrDeg)
            case .pi:
                return Double.pi
            case .sign:
                return Number * (-1)
            default:
                print ("Используйте Стандартный калькулятор")
                return 0
            }
        }
    }
}

func CalculOper(_ object: Calcul) -> Double {
    return object.Chet()
}

var Del = CalculOper(Calculator(Number: 10.0, NumOrDeg: 3.0, operation: .divis, calculator: .Standart))
var Mul = CalculOper(Calculator(Number: 10, NumOrDeg: 3, operation: .multi, calculator: .Standart))
var Plu = CalculOper(Calculator(Number: 10, NumOrDeg: 3, operation: .plus, calculator: .Standart))
var Min = CalculOper(Calculator(Number: 10, NumOrDeg: 3, operation: .minus, calculator: .Standart))
var Square = CalculOper(Calculator(Number: 10, NumOrDeg: 3, operation: .square, calculator: .Special))
var Degree = CalculOper(Calculator(Number: 10, NumOrDeg: 3, operation: .degree, calculator: .Special))
var Pi = CalculOper(Calculator(Number: 10, NumOrDeg: 3, operation: .pi, calculator: .Special))
var SignPlus = CalculOper(Calculator(Number: 10, NumOrDeg: 3, operation: .sign, calculator: .Special))
var SignMinus = CalculOper(Calculator(Number: -10, NumOrDeg: 3, operation: .sign, calculator: .Special))

var ErrorSpecial = CalculOper(Calculator(Number: 10, NumOrDeg: 3, operation: .minus, calculator: .Special))
var ErrorStandart = CalculOper(Calculator(Number: 10, NumOrDeg: 3, operation: .square, calculator: .Standart))
