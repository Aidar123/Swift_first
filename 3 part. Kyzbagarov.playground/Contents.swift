import UIKit

// Задание 1
func fibonacciNumber (count: Int) {
    var Num1 = 1
    var Num2 = 1
    var storyPointsNumbers: [Int] = [1,1]
    for _ in 0 ..< count-2 {
        let Num = Num1 + Num2
        Num1 = Num2
        Num2 = Num
        storyPointsNumbers.append (Num)
    }
    for i in 0 ..< count {
    print (storyPointsNumbers[i])
    }
}
print("Задание 1")
fibonacciNumber (count: 5)
print("___________")
fibonacciNumber (count: 7)

// Задание 2
var Mas = [1,5,6,8,13]
let printArrayFor = {() -> Int in
    for A in 0 ..< Mas.count {
    print (Mas[A])
    }
    return 0
}
print ("Задание 2")
let res = printArrayFor()
