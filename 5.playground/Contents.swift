import UIKit

// Задание 1
class Shape {
    func calculateArea() -> Double {
        return calculateArea()
    }
    func calculatePerimeter() -> Double {
        return calculatePerimeter()
    }
}

class Rectangle: Shape {
    private let height: Double
    private let width: Double
    override func calculateArea() -> Double {
        return height * width
    }
    override func calculatePerimeter() -> Double {
        return 2*(height+width)
    }
    init(height : Double, width : Double) {
        self.height = height
        self.width = width
    }
}

class Circle: Shape {
    private let radius: Double
    override func calculateArea() -> Double {
        return Double.pi * radius * radius
    }
    override func calculatePerimeter() -> Double {
        return 2 * Double.pi * radius
    }
    init(radius : Double) {
        self.radius = radius
    }
}

class Square: Shape {
    private let side: Double
    override func calculateArea() -> Double {
        return side * side
    }
    override func calculatePerimeter() -> Double {
        return 2 * side
    }
    init(side : Double) {
        self.side = side
    }
}


var Shapes: [Shape] = [Rectangle(height: 5.0, width: 5.0), Circle(radius: 2.0), Square(side: 5.0)]

print (Shapes[0].calculateArea() + Shapes[1].calculateArea() + Shapes[2].calculateArea() )
print(Shapes[0].calculatePerimeter() + Shapes[1].calculatePerimeter() + Shapes[2].calculatePerimeter())

/*
// Для проверки что все считается правильно
var RecArea = Rectangle(height: 5.0, width: 5.0)
RecArea.calculateArea()
var RecPer = Rectangle(height: 5.0, width: 5.0)
RecPer.calculatePerimeter()

var CircleArea = Circle(radius: 2.0)
CircleArea.calculateArea()
var CirclePer = Circle(radius: 2.0)
CirclePer.calculatePerimeter()

var SquareArea = Square(side: 5.0)
SquareArea.calculateArea()
var SquarePer = Square(side: 5.0)
SquarePer.calculatePerimeter()

var SumArea = RecArea.calculateArea() + CircleArea.calculateArea() + SquareArea.calculateArea()
var SumPer = RecPer.calculatePerimeter() + CirclePer.calculatePerimeter() + SquarePer.calculatePerimeter()

*/

// Задание 2
func findIndexWithGenerics <T> (valueToFind: T, in array: [T]) -> Int? where T: Equatable {

    for (index, value) in array.enumerated() {

        if value == valueToFind {

            return index

        }

    }

    return nil

}

 

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]

if let foundIndexString = findIndexWithGenerics(valueToFind: "лама", in: arrayOfString) {

    print("Индекс ламы: \(foundIndexString)")

}

let arrayOfDouble = [5.0, 4.7, 2.1, 0.4, 7.0]

if let foundIndexDouble = findIndexWithGenerics(valueToFind: 5.0, in: arrayOfDouble) {

    print("Индекс 5.0: \(foundIndexDouble)")

}

let arrayOfInt = [1, 5, 75, 8, 11]

if let foundIndexInt = findIndexWithGenerics(valueToFind: 8, in: arrayOfInt) {

    print("Индекс 8: \(foundIndexInt)")

}
