import UIKit
import CoreGraphics
// Задание 1 3 4
enum Priority: String {
    case High = "High"
    case Midle = "Midle"
    case Low = "Low"
}
class Form {
    let FieldHeader: String

    var FieldLength: Int

    var FieldPlaceholder: String?

    var FieldCode: Int?

    var FieldPriority: Priority
        

    
    init (FieldHeader: String, FieldLength: Int, FieldPlaceholder: String? = "No", FieldCode: Int? = 0, FieldPriority: Priority) {
        self.FieldHeader = FieldHeader
        self.FieldLength = FieldLength
        self.FieldPlaceholder = FieldPlaceholder
        self.FieldCode = FieldCode
        self.FieldPriority = FieldPriority
        if FieldPriority == .High {
            print(FieldPriority.rawValue)
        } else if FieldPriority == .Midle {
            print(FieldPriority.rawValue)
        } else {
            print(FieldPriority.rawValue)
        }
    }
    func ManySimbol () {
        var FieldLengthFlag = true
        if(FieldLength > 25) {
            FieldLengthFlag = false
            print(FieldLengthFlag)
        }
        else {
            print(FieldLengthFlag)

        }
    }
}
var NameCharacters = Form(FieldHeader: "Name", FieldLength: 25, FieldPlaceholder: "Type your name", FieldCode: 1, FieldPriority: .High)

print(NameCharacters.FieldHeader)
print(NameCharacters.FieldLength)
if NameCharacters.FieldPlaceholder == nil {
    print("0")

}
else {
    print(NameCharacters.FieldPlaceholder!)

}
if NameCharacters.FieldCode == nil {
    print("0")

}
else {
    print(NameCharacters.FieldCode!)

}
print(NameCharacters.FieldPriority)

// задание 2
var NameCharacters2 = Form(FieldHeader: "Name", FieldLength: 30, FieldPlaceholder: "Type your name", FieldCode: 1, FieldPriority: .High)

NameCharacters2.ManySimbol()
NameCharacters2 = Form(FieldHeader: "Name", FieldLength: 10, FieldPlaceholder: "Type your name", FieldCode: 1, FieldPriority: .High)
NameCharacters2.ManySimbol()


// задание 3
var NameCharacters3 = Form(FieldHeader: "Name", FieldLength: 30, FieldCode: 1, FieldPriority: .High)

// задание 5

struct Form2 {
    let FieldHeader: String

    var FieldLength: Int

    let FieldPlaceholder: String

    let FieldCode: Int
    
    init (FieldHeader: String, FieldLength: Int, FieldPlaceholder: String, FieldCode: Int) {
        self.FieldHeader = FieldHeader
        self.FieldLength = FieldLength
        self.FieldPlaceholder = FieldPlaceholder
        self.FieldCode = FieldCode
    func ManySimbol (FieldLength: Int) {
        if(FieldLength > 25) {
            print("Длинна поля не может быть больше 25 символов")
            
        }
        else {
            print(FieldLength)

        }
    }
  }
}

var NameCharacters5 = Form2(FieldHeader: "Struct", FieldLength: 5, FieldPlaceholder: "Type is Struct", FieldCode: 2)

print ("Задание 5")
print(NameCharacters5.FieldHeader)
print(NameCharacters5.FieldLength)
print(NameCharacters5.FieldPlaceholder)
print(NameCharacters5.FieldCode)

