import UIKit

// Задание 1

extension Int {
    func SunCifr (by Number: Int) -> Int {
        var sum: Int = 0
        var ost: Int = Number
        while (ost != 0) {
            sum += ost % 10
            ost = ost / 10
        }
        return sum
    }
}

123.SunCifr(by: 123)
731.SunCifr(by: 731)

// Задание 2
extension String {
    func StrInInt (by Str: String) -> Int? {
        let Nu = Int(Str)
        if Nu == nil {
            return 0
        } else {
            return Nu
        }
    }
}

var StrNum: String = "123"
StrNum.StrInInt(by: "104")
StrNum.StrInInt(by: "53")
StrNum.StrInInt(by: "920")
StrNum.StrInInt(by: "Text")

// Задание 3
enum Operation: Double {
    case multi = 0
    case divis = 1
}
protocol Calcul {
    func Chet() -> Double
}

class Multi: Calcul {
    let First: Double
    let Two: Double
    init(First: Double, Two: Double) {
        self.First = First
        self.Two = Two
    }
    func Chet() -> Double {
        return First * Two
    }
}

class Division: Calcul {
    let First: Double
    let Two: Double
    init(First: Double, Two: Double) {
        self.First = First
        self.Two = Two
    }
    func Chet() -> Double {
        return First / Two
    }
}

class Calculator: Calcul {   
    let First: Double
    let Two: Double
    let operation: Double
    init(First: Double, Two: Double, operation: Double) {
        self.First = First
        self.Two = Two
        self.operation = operation
    }
    func Chet() -> Double {
        if operation == 0 {
            return First * Two
        } else if operation == 1 {
            return First / Two
        } else {
            return 0
        }
    }
}

func CalculOper(_ object: Calcul) -> Double {
    return object.Chet()
}

var Del = Calculator(First: 10.0, Two: 3.0, operation: Operation.divis.rawValue)
print (Del.Chet())
var Mul = Calculator(First: 10, Two: 3, operation: Operation.multi.rawValue)
print (Mul.Chet())


