import UIKit
//Задача 1
var milkmanPhrase = "Молоко - это полезно"

//Задача 2
let milkPrice: Int = 3
//Задача 3
var DoubleMilkPrice = Double(milkPrice)
DoubleMilkPrice = 4.20

//Задача 4
var milkBottleCount: Int? = 20
var profit = 0.0
profit = DoubleMilkPrice * Double(milkBottleCount!)
print(profit)
//Доп задание
if milkBottleCount != nil {
    profit = DoubleMilkPrice * Double(milkBottleCount!)
    print(profit)
} else {
    print("Переменная milkBottleCount == nil ")
}
// при принудительном развертывании система может сломаться, если мы попытаемся развернуть nil

//Задача 5
let employeesList = ["Иван","Марфа","Андрей","Петра","Генадий"]

//Задача 6
var isEveryoneWorkHard = false
var workingHours = 20
if workingHours >= 40{
    isEveryoneWorkHard = true
} else {
    isEveryoneWorkHard = false
}
print(isEveryoneWorkHard)


